

<div id="top-campaigns-menu-hide">
	<div id="top-campaigns-menu">

		<div id="top-campaigns-menu-inner">
			<div id="top-campaign-boxes-main">
				<div id="getmore-more" class="top-get-more-boxes">
					<?php getMore(); ?>
				</div>

				<div id="getmore-live" class="top-get-more-boxes">
					<?php getMore('SHOUT LIVE'); ?>
				</div>

				<div id="getmore-connect" class="top-get-more-boxes">
					<?php getMore('SHOUT ACTIVATE'); ?>
				</div>

				<div id="getmore-motion" class="top-get-more-boxes">
					<?php getMore('MOTION'); ?>
				</div>

				<div id="getmore-convenience" class="top-get-more-boxes">
					<?php getMore('CONVENIENCE'); ?>
				</div>

				<div id="getmore-street" class="top-get-more-boxes">
					<?php getMore('STREET'); ?>
				</div>

				<div id="getmore-media" class="top-get-more-boxes">
					<?php getMore('CAFE MEDIA'); ?>
				</div>

				<div id="getmore-air" class="get-more-boxes">
					<?php getMore('AIR NEW ZEALAND'); ?>
				</div>
			</div>
		</div>
	</div>
</div>







<?php
function getMore($cat){

	?>
	<?php

	$args = array( 'post_type' => 'Campaigns', 
					'category_name'    => $cat,
					'orderby' => 'date',
					'order' => 'ASC');
	$loop = new WP_Query( $args );

	$count = count( get_posts( array( 
		'post_type' => 'Campaigns', 
							    'nopaging'  => true, // display all posts
							    ) ) );

	$count_posts = wp_count_posts('campaigns');
	$number_of_posts = $count_posts->publish; 

	$layer_count = 0;
	$mycount = 0;
	$campaign_number = 0;
	$layer = 0;

	if($cat == 'SHOUT LIVE'){
		$catname = "LIVE";
	}elseif ($cat == 'SHOUT CONNECT') {
		$catname = "CONNECT";
	}elseif ($cat == 'MOTION') {
		$catname = "MOTION";
	}elseif ($cat == 'CONVENIENCE') {
		$catname = "CONVENIENCE";
	}elseif ($cat == 'STREET') {
		$catname = "STREET";
	}elseif ($cat == 'CAFE MEDIA') {
		$catname = "MEDIA";
	}elseif ($cat == 'AIR NEW ZEALAND') {
		$catname = "AIR";
	}
	else{
		$catname = "MORE";
	}


	while ( $loop->have_posts() ) : $loop->the_post();

	if ($layer_count == 0){
		if($campaign_number == 0){
			echo "<div class='campaigns-boxes' id='campaign-boxes-".$campaign_number.$catname."' style='display:block'>";
		}
		else{
			echo "<div class='campaigns-boxes' id='campaign-boxes-".$campaign_number.$catname."' style='display:none'>";
		}
	}

	echo "<a href='".get_permalink()."'>";
	echo "<div class='campaigns-box'>";
	echo "<div class='campaigns-text'>";
	the_title();
	echo "</div>";
	echo "</div>";
	echo "</a>";	
	$mycount++;
	$layer_count++;

	if($mycount == 14){
		echo "<div class='campaigns-box-transparent'>";
		echo "<div class='campaigns-text'>";
		echo "<div class='cat-nav'>";

		if($campaign_number != 0){
			echo "<div class='cat-prev' id='".$campaign_number."' cat='".$catname."'><img class='arrows' src='/wp-content/uploads/2016/05/arrow_prev_pink.png'></div>";
			$new_number_of_posts = $number_of_posts - (14 * $campaign_number);
			if ($new_number_of_posts > 14){
				if ($mycount == 14){
					echo "<div class='cat-next' id='".$campaign_number."' cat='".$catname."'><img class='arrows' src='/wp-content/uploads/2016/05/arrow_next_pink.png'></div>";
				}
			}
		}
		else{
			if($number_of_posts > 14){
				echo "<div class='cat-next'><img class='arrows' src='/wp-content/uploads/2016/05/arrow_prev_no_pink.png'></div>";
				echo "<div class='cat-next' id='".$campaign_number."' cat='".$catname."'><img class='arrows' src='/wp-content/uploads/2016/05/arrow_next_pink.png'></div>";
			}
		}


								//echo $mycount;

		echo "</div>";
		echo "</div>";
		echo "</div>";

		echo "</div>";
		$mycount = 0;
		$campaign_number++;
		$layer_count = 0;
	}


	if($mycount == $number_of_posts){
		$blanks = 14 - $mycount;
		for ($i=0; $i < $blanks; $i++) { 
			echo "<div class='campaigns-box-blank'>";
			echo "<div class='campaigns-text'>";
			echo "</div>";
			echo "</div>";

		}

		echo "<div class='campaigns-box-blank'>";
		echo "<div class='campaigns-text'>";
								//echo "<div class='cat-nav'>";
								//if($campaign_number != 0){
								//	echo "<div class='cat-prev' id='".$campaign_number."'>PREV</div>";
								//}
								//echo "<div class='cat-next' id='".$campaign_number."'>NEXT</div>";
								//echo "</div>";
		echo "</div>";
		echo "</div>";


		echo "</div>";
		$mycount = 0;
		$layer_count = 0;	
		$stop = 0;	
		$campaign_number++;						
	}



	endwhile;

	$num = $mycount % 14;

	if($num != 0 && $number_of_posts > 14){
		$blanks = 14 - $mycount;
		for ($i=0; $i < $blanks; $i++) { 
			echo "<div class='campaigns-box-blank'>";
			echo "<div class='campaigns-text'>";
			echo "</div>";
			echo "</div>";

		}

		if($campaign_number != 0){
			echo "<div class='campaigns-box-transparent'>";
			echo "<div class='campaigns-text'>";
			echo "<div class='cat-nav'>";
			echo "<div class='cat-prev' id='".$campaign_number."' cat='".$catname."'><img class='arrows' src='/wp-content/uploads/2016/05/arrow_prev_pink.png'></div>";
			echo "<div class='cat-prev'><img class='arrows' src='/wp-content/uploads/2016/05/arrow_next_no_pink.png'></div>";
			echo "</div>";
		}else{
			echo "<div class='campaigns-box-blank'>";
			echo "<div class='campaigns-text'>";			
		}
								//echo "<div class='cat-next' id='".$campaign_number."'>NEXT</div>";

		echo "</div>";
		echo "</div>";

		echo "</div>";
		$layer_count = 0;	
		$campaign_number++;							
	}


	?>


	<?php
}
?>


<?php

