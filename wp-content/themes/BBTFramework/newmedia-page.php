<?php
/**
 * The template used for displaying page content in media_template.php
 *
 * @package sparkling
 */
?>

<div class="media_page">
<div class="post-inner-content">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div id="main_media">
	<div id="main_media_image">
	        <?php the_post_thumbnail( 'full', array( 'class' => 'single-featured' )); ?>
	 </div>
	 <div class="main_media_text">

	 		<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'sparkling' ),
				'after'  => '</div>',
			) );
		?>
    <?php
      // Checks if this is homepage to enable homepage widgets
      if ( is_front_page() ) :
        get_sidebar( 'home' );
      endif;
    ?>
	</div><!-- .entry-content -->
	 </div>
</div>

<div id="main_media_small">


	<div id="media-featured-image">
		<?php the_post_thumbnail( 'full', array( 'class' => 'single-featured' )); ?>
	</div>


	<div id="media-featured-title">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</div>


	<div id="media-featured-content">
			<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'sparkling' ),
				'after'  => '</div>',
			) );
		?>
    <?php
      // Checks if this is homepage to enable homepage widgets
      if ( is_front_page() ) :
        get_sidebar( 'home' );
      endif;
    ?>
	</div><!-- .entry-content -->
	</div>
</div>


</article><!-- #post-## -->

<div id="bottom-menu">
              <div class="navigation-small">
                    <div id="prev-small"><?php previous_post('%','<img class="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue_prev.png">', 'no'); ?> </div>
                    <div id="small-menu"><div id="small-menu-campaigns"><img id="small-menu-image" src="/wp-content/uploads/2015/11/small-menu.png"></div></div>
                    <div id="small-menu-2"><div id="small-menu-campaigns-2"><img id="small-menu-image-2" src="/wp-content/uploads/2015/04/small-menu-2.png"></div></div>                    
                    <div id="next-small"><?php next_post('%','<img class="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue_next.png">', 'no'); ?></div>
               </div>               
            </div>


                            <!--Campaigns Carousel-->

                            <div id="campaign-carousel-main">
                              <div id="campaign-carousel-inner">
                                  <?php
                                    if(is_singular( 'squad' )){
                                        $args = array( 'post_type' => 'SQUAD');
                                      }else{
                                        $args = array( 'category_name' => 'MEDIA');
                                      }
                                        $loop= new WP_Query( $args );
                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            echo "<a href='".get_permalink()."'>";
                                            echo "<div class='campaign-carousel-text'>";
                                            the_title();
                                            echo "</div>";
                                            echo "</a>";
                                        endwhile;
                                  ?>
                                </div>
                            </div>


                            <!--<div id="campaign-carousel-main">
                                <div class="campaign-carousel">
                                  <?php

                                      if(is_singular( 'squad' )){
                                        $args = array( 'post_type' => 'SQUAD');
                                      }else{
                                        $args = array( 'category_name' => 'MEDIA');
                                      }
                                        $loop= new WP_Query( $args );

                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            echo "<a href='".get_permalink()."'>";
                                            echo "<div class='campaign-carousel-text'>";
                                            the_title();
                                            echo "</div>";
                                        endwhile;
                                    ?>
                                </div>
                            </div>-->
                            <!--END of Carousel-->



<?php //include('contact.php'); ?>

</div>
</div>