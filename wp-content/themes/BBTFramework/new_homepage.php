<?php
/**
 * Template Name: NewHomepage
 *
 * This is the template for new homepage
 *
 * @package sparkling
 */

get_header(); ?>

<style type="text/css">
.featured-image{
	width: 100%;
}

.post-inner-content {
    width: 40% !important;
    padding: 0;
    position: absolute;
    left: 5%;
    height: 100%;
    background-color: rgba(255,0,153,0.75);
}

.entry-content{
	padding: 12%;
    padding-top: 18%;
}

.post-inner-content h1{
	font-size: 45px;
	text-shadow: 2px 2px 0 #009FDA, -2px 2px 0 #009FDA, 2px -2px 0 #009FDA, -2px -2px 0 #009FDA, 0px 2px 0 #009FDA, 0px -2px 0 #009FDA, -2px 0px 0 #009FDA, 2px 0px 0 #009FDA, 3px 3px 0 #009FDA, -3px 3px 0 #009FDA, 3px -3px 0 #009FDA, -3px -3px 0 #009FDA, 0px 3px 0 #009FDA, 0px -3px 0 #009FDA, -3px 0px 0 #009FDA, 3px 0px 0 #009FDA, 2px 3px 0 #009FDA, -2px 3px 0 #009FDA, 2px -3px 0 #009FDA, -2px -3px 0 #009FDA, 3px 2px 0 #009FDA, -3px 2px 0 #009FDA, 3px -2px 0 #009FDA, -3px -2px 0 #009FDA;
}

.post-inner-content p {
    font-family: 'Aleo-Regular';
    font-size: 35px;
    font-weight: 600;
    padding-top: 5%;
}

#findoutmore p{
	font-family: 'Aleo-Regular';
    font-weight: 600;
    color: #ffffff;
    font-size: 16px;
}

#findoutmore a:hover {
    color: #009FDA !important;
}




/** RESPONSIVENESS **/
@media (max-width: 1024px){
    .post-inner-content h1{
        font-size: 35px;
    }
    .post-inner-content p{
        font-size: 25px;
    }
}

@media (max-width: 992px){
    .post-inner-content{
        width: 100% !important;
        left:0;
        position: relative;
        background-color: rgba(255,0,153,1);
    }

    .entry-content{
        padding: 5%;
    }

    .post-inner-content p{
        padding-top: 2%;
    }
}

</style>


  <div id="primary" class="content-area">

    <main id="main" class="site-main" role="main" style="height: auto">

    				<?php //include('sidepanel.php'); ?>

    				<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'new_homepage', 'single' ); ?>

					<?php endwhile; // end of the loop. ?>


    </main><!-- #main -->


  </div><!-- #primary -->


<?php

?>

<?php get_footer(); ?>