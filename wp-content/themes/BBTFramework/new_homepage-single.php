<?php
/**
 * @package sparkling
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="featured-image">
		<?php the_post_thumbnail( 'full', array( 'class' => 'single-featured' )); ?>
	</div>

	<div class="post-inner-content" style="width: 100%; padding: 0;">

		<div class="entry-content" style="max-height: none; margin: 0;">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before'            => '<div class="page-links">'.__( 'Pages:', 'sparkling' ),
					'after'             => '</div>',
					'link_before'       => '<span>',
					'link_after'        => '</span>',
					'pagelink'          => '%',
					'echo'              => 1
	       		) );
	    	?>
		</div>
		<!-- .entry-content -->

		<footer class="entry-meta">

	    	<?php if(has_tag()) : ?>
	      <!-- tags -->
	      <div class="tagcloud">

	          <?php
	              $tags = get_the_tags(get_the_ID());
	              foreach($tags as $tag){
	                  echo '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a> ';
	              } ?>

	      </div>
	      <!-- end tags -->
	      <?php endif; ?>

		</footer><!-- .entry-meta -->





                            <!--Campaigns Carousel-->
                            <div id="campaign-carousel-main">
                            	<div id="campaign-carousel-inner">
                                	<?php
                                		$args = array( 'post_type' => 'Campaigns');
                                        $loop= new WP_Query( $args );
                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            echo "<a href='".get_permalink()."'>";
                                            echo "<div class='campaign-carousel-text'>";
                                            the_title();
                                            echo "</div>";
                                            echo "</a>";
                                        endwhile;
                                	?>
                                </div>
                            </div>


	</div>



</article><!-- #post-## -->







