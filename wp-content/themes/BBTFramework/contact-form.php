<?php
/**
 * Template Name: Contact
 *
 * This is the template for campaigns
 *
 * @package sparkling
 */


?>
<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<script src="javascript.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>

<?php wp_head(); ?>


<style>

html{
	margin: 0 !important;
}

body{
	margin: 0;
	background-color: #009FDA;
}
ul#gform_fields_1 {
  list-style-type: none;
  padding: 0;
}
label.gfield_label {
  float: left;
  width: 70px;
  font-weight: 500;
}

#contact-form-inner{
	color: #ffffff;
  font-family: 'Aleo-Regular';
  font-size: 16px;
}

input#input_1_1, input#input_1_2, input#input_1_3 {
  background-color: transparent;
  border: 0;
  border-bottom: 2px dotted white;
  width: 280px;
  padding: 0;
  color: #ffffff;
  font-size: 15px;
  font-family: 'Aleo-Regular';
}

textarea#input_1_4 {
  width: 99%;
  resize: none;
  background-color: transparent;
  border: 0;
  border: 2px dotted white;
  margin-top: 1%;
    height: 60px;
  color: #ffffff;
  font-size: 15px;
  font-family: 'Aleo-Regular';
}

li {
  padding-top: 1.5%;
  height: 40px;
}

li#field_1_4{
	height: 100px;
}

#required {
  position: absolute;
  right: 10px;
   bottom: 10px;
}

input#gform_submit_button_1 {
  background-color: #ff0099;
  border: 0;
  color: #ffffff;
  padding: 10%;
  padding-top: 2%;
  padding-bottom: 2%;
  font-family: Aleo-Regular;
  text-transform: uppercase;
  margin-top: 2%;
}

textarea:focus, input:focus{
    outline: 0;
}

.validation_error{
	display: none;
}

.gfield_description.validation_message, .instruction.validation_message {
position: relative;
  top: -3px;
  font-size: 12px;
  left: 0px;
}


</style>

</head>

<div id="contact-form-inner">
    						<?php
    							gravity_form( "1", $display_title = false, $display_description = false, $ajax = false, $tabindex );
    						?>
    						<div id="required">* Required fields</div>
</div>