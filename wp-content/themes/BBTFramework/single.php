<?php
/**
 * The Template for displaying all single posts.
 *
 * @package sparkling
 */

get_header(); ?>

    <div id="primary" class="content-area">

<?php
if(in_category('media')) {
?>

<div id="media-next-main">
                <div id="media-next-inner-main">
                MORE
                </div>
            </div>

        <div id="media-menu">
            
            <div id="media-next">
                <div id="media-next-inner">
                <img id="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue.png">
                <img id="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue.png">
                </div>
            </div>
                
            <div id="media-boxes">
                    
                    <a href='/street-posters/'>
                    <div id="media-box">
                        <div class="media-text">STREET POSTERS</div>
                    </div>
                    </a>

                    <a href='/digital-fuel-network/'>
                    <div id="media-box">
                        <div class="media-text">DIGITAL FUEL NETWORK</div>
                    </div>
                    </a>


                    <a href='/convenience/'>
                    <div id="media-box">
                        <div class="media-text">PATH TO PURCHASE</div>
                    </div>
                    </a>

                    <a href='/air-new-zealand/'>
                    <div id="media-box">
                        <div class="media-text">AIR NEW ZEALAND</div>
                    </div>
                    </a>

                    <a href='/street/'>
                    <div id="media-box">
                        <div class="media-text">STREET</div>
                    </div>
                    </a>

                    <a href='/cafe-media/'>
                    <div id="media-box">
                        <div class="media-text">CAFE MEDIA</div>
                    </div>
                    </a>

                    <a href='/motion/'>
                    <div id="media-box">
                        <div class="media-text">MOTION</div>
                    </div>
                    </a>

                    <a href='/shout-connect/'>
                    <div id="media-box">
                        <div class="media-text">SHOUT ACTIVATE</div>
                    </div>
                    </a>


                    <!--<a href='/shout-live/'>
                    <div id="media-box">
                        <div class="media-text">SHOUT LIVE</div>
                    </div>
                    </a>-->
                    
            </div>

        </div>
<?php
}
?>



        <main id="main" class="site-main" role="main">

            <?php while ( have_posts() ) : the_post(); ?>

                <?php get_template_part( 'media', 'page' ); ?>

            <?php endwhile; // end of the loop. ?>

        </main><!-- #main -->


    </div><!-- #primary -->




<?php get_footer(); ?>
