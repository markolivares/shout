<?php
/**
 * Template Name: Media
 *
 * This is the template for media page
 *
 * @package sparkling
 */

get_header(); ?>

<style>

#bottom-menu {
  display: none;
}

@media (max-width: 768px){
#inner-layer-black, #inner-layer{
        height: 1210px;
    }
}

@media (max-width: 480px){
    #inner-layer-black{
        height: 700px;
    }
    #inner-layer{
        height:700px;
    }

    .media_image img{
        width:80%;
    }

    #contact-form {
        top: 40%;
    }
}

</style>

<div id="primary" class="content-area">

    <main id="main" class="site-main" role="main" style="height:100%; background-color:#00d9ff;">

        <div id="media_content">

             <div class="shout_media">
                <div class="media_image">
                    <img src="/wp-content/uploads/2016/04/posters1_4.png">
                </div>

                <div class="media_text">  
                    <div class="media_title" id="media_title_purple">
                        STREET POSTERS
                    </div>
                    <div class="media_description">
                        Street Posters are by far the most cost effective mainstream outdoor media available. They are highly visible and target both ...
                    </div>
                     <div class="media_more">
                         <a href='/street-posters/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>

            </div>

            <div class="shout_media">
                <div class="media_image">
                    <img src="/wp-content/uploads/2016/04/fuel1_1.png">
                </div>

                <div class="media_text">
                    <div class="media_title" id="media_title_yellow">
                        DIGITAL FUEL NETWORK
                    </div>
                    <div class="media_description">
                        Shout Media is the exclusive provider of New Zealand’s most comprehensive digital out of home media network ...
                    </div>
                    <div class="media_more">
                         <a href='/digital-fuel-network/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>

            </div>

            <div class="shout_media">
                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/convenience1.png">
                </div>

                <div class="media_text">
                    <div class="media_title" id="media_title_yellow">
                        PATH TO PURCHASE
                    </div>
                    <div class="media_description">
                        Dairies are an everyday part of New Zealanders lives, located on most busy arterials in every city and town throughout the country...
                    </div>
                    <div class="media_more">
                         <a href='/convenience/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>

            </div>

            <div class="shout_media">

                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/air_new_zealand.png">
                </div>

                <div class="media_text">  
                    <div class="media_title" id="media_title_purple">
                        AIR NEW ZEALAND
                    </div>
                    <div class="media_description">
                        Air New Zealand delivers an exclusive and sought after audience in New Zealand, we have the ability to achieve a depth of engagement  that few ...
                    </div>
                     <div class="media_more">
                         <a href='/air-new-zealand/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>
            </div>

            <div class="shout_media">
                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/street.png">
                </div>

                <div class="media_text">
                    <div class="media_title" id="media_title_pink">
                        STREET
                    </div>
                    <div class="media_description">
                        We have a number of different options for guerrilla message on the pavement. Street Stencils should be cost effective;  our quotes include production...
                    </div>
                  <div class="media_more">
                         <a href='/street/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>

            </div>


            <div class="shout_media">
                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/cafe_media.png">
                </div>

                <div class="media_text">
                    <div class="media_title" id="media_title_green">
                        CAFE MEDIA
                    </div>
                    <div class="media_description">
                       Shout is the leading supplier of advertising in NZ cafes. Options include branded takeaway cups, decals on lids and branded lunch bags ...
                    </div>
                    <div class="media_more">
                         <a href='/cafe-media/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>

            </div>

            <div class="shout_media">

                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/motion11.png">
                </div>

                <div class="media_text">  
                    <div class="media_title" id="media_title_purple">
                        MOTION
                    </div>
                    <div class="media_description">
                       Vehicles that move providing us with reach.  We have a wide range of mediums that allows us nationwide coverage. Our different types ...
                    </div>
                     <div class="media_more">
                         <a href='/motion/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>
            </div>


            <div class="shout_media">
                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/shout_connect.png">
                </div>

                <div class="media_text">
                    <div class="media_title" id="media_title_green">
                        SHOUT ACTIVATE
                    </div>
                    <div class="media_description">
                        Need to get your client’s product in front of the right demographic? Whether it’s placing a vehicle in corporate towers ...
                    </div>
                    <div class="media_more">
                         <a href='/shout-connect/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>

            </div>

            <!--<div class="shout_media">
                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/shout_engage.png">
                </div>

                <div class="media_text">
                    <div class="media_title" id="media_title_pink">
                        SHOUT LIVE
                    </div>
                    <div class="media_description">
                        We bring to life fabulous experiences for consumers.  With teams in Auckland, Wellington & Christchurch we have the ability to roll ...
                    </div>
                  <div class="media_more">
                         <a href='/shout-live/'>FIND OUT MORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>></a>
                    </div>
                </div>

            </div>-->

    </div>




    <div id="media_content-small">

        <a href='/street-posters/'><div class="media-box-small" style="border-right: 2px solid #ffffff">
                <div class="media_image">
                    <img src="/wp-content/uploads/2016/04/posters1_4.png">
                </div>
                <div class="media_title">
                        STREET POSTERS
                </div>
        </div></a>

        <div class="media-box-small" style="border-left: 2px solid #ffffff">
            <div class="media_image">
                   <img src="/wp-content/uploads/2016/04/fuel1_1.png">
                </div>
                <div class="media_title">
                        DIGITAL FUEL NETWORK
                </div>
        </div>

        <a href='/convenience/'><div class="media-box-small" style="border-right: 2px solid #ffffff">
            <div class="media_image">
                   <img src="/wp-content/uploads/2015/11/convenience1.png">
                </div>
                <div class="media_title">
                        PATH TO PURCHASE
                </div>
        </div></a>

        <a href='/air-new-zealand/'><div class="media-box-small" style="border-left: 2px solid #ffffff;">
            <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/air_new_zealand.png">
                </div>
                <div class="media_title">
                        AIR NEW ZEALAND
                </div>
        </div></a>

        <a href='/street/'><div class="media-box-small" style="border-right: 2px solid #ffffff">
            <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/street.png">
                </div>
                <div class="media_title">
                        STREET
                </div>
        </div></a>

        <a href='/cafe-media/'><div class="media-box-small" style="border-left: 2px solid #ffffff">
            <div class="media_image">
                   <img src="/wp-content/uploads/2015/11/cafe_media.png">
                </div>
                <div class="media_title">
                        CAFE MEDIA
                </div>
        </div></a>

        <a href='/motion/'><div class="media-box-small" style="border-right: 2px solid #ffffff">
            <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/motion12.png">
                </div>
                <div class="media_title">
                        MOTION
                </div>
        </div></a>

        <a href='/shout-connect/'><div class="media-box-small" style="border-left: 2px solid #ffffff">
                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/shout_connect.png">
                </div>
                <div class="media_title">
                        SHOUT CONNECT
                </div>
        </div></a>

        <!--<a href='/shout-live/'><div class="media-box-small" style="border-left: 2px solid #ffffff">
                <div class="media_image">
                    <img src="/wp-content/uploads/2015/11/shout_engage.png">
                </div>
                <div class="media_title">
                        SHOUT LIVE
                </div>
        </div></a>-->
        
    </div>


    <?php include('contact.php'); ?>

</main><!-- #main -->

</div><!-- #primary -->

<?php get_footer(); ?>