<?php
/**
 * @package sparkling
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="right-content">
		<div class="navigation">
			<div id="topmenu">
				<?php //include('toppanel.php'); ?>
			</div>
			<div id="next-prev-menu-link">
				<span id="next"><?php next_post('%','NEXT  <img class="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue_next.png">', 'no'); ?></span>
				<span id="prevnext-center"><img id="prevnext-center-image" src="/wp-content/uploads/2015/11/small-menu.png"><img id="prevnext-center-image-close" src="/wp-content/uploads/2015/11/small-menu.png"></span>
				<span id="prev"><?php previous_post('%','<img class="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue_prev.png">  PREVIOUS', 'no'); ?> </span>
			</div>
		</div>
		<div class="featured-image">
			<?php the_post_thumbnail( 'full', array( 'class' => 'single-featured' )); ?>
		</div>
	</div>

	<div class="post-inner-content">
		<header class="entry-header page-header">

			<h1 class="entry-title "><?php the_title(); ?></h1>

			<div class="view-more">
				<div id="view-more-from">VIEW MORE FROM:</div>
				<?php $category = get_the_category(); ?>
				<div id="category-name"><?php echo $category[0]->cat_name; ?></div>			
			</div>

		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before'            => '<div class="page-links">'.__( 'Pages:', 'sparkling' ),
					'after'             => '</div>',
					'link_before'       => '<span>',
					'link_after'        => '</span>',
					'pagelink'          => '%',
					'echo'              => 1
	       		) );
	    	?>
		</div><!-- .entry-content -->

		<footer class="entry-meta">

	    	<?php if(has_tag()) : ?>
	      <!-- tags -->
	      <div class="tagcloud">

	          <?php
	              $tags = get_the_tags(get_the_ID());
	              foreach($tags as $tag){
	                  echo '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a> ';
	              } ?>

	      </div>
	      <!-- end tags -->
	      <?php endif; ?>

		</footer><!-- .entry-meta -->



		


            <div id="bottom-menu">
            	<div class="navigation-small">
                    <div id="prev-small"><?php previous_post('%','<img class="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue_prev.png">', 'no'); ?> </div>
                    <div id="top-small-menu" class="campaign-small-menu"><div id="small-menu-campaigns"><img id="small-menu-image" src="/wp-content/uploads/2015/11/small-menu.png"></div></div>
                    <div id="top-small-menu-2" class="campaign-small-menu-2"><div id="small-menu-campaigns-2"><img id="small-menu-image-2" src="/wp-content/uploads/2015/04/small-menu-2.png"></div></div>                    
                    <div id="next-small"><?php next_post('%','<img class="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue_next.png">', 'no'); ?></div>
               </div>               
            </div>

                            <!--Campaigns Carousel-->
                            <div id="campaign-carousel-main">
                            	<div id="campaign-carousel-inner">
                                	<?php
                                		$args = array( 'post_type' => 'Campaigns');
                                        $loop= new WP_Query( $args );
                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            echo "<a href='".get_permalink()."'>";
                                            echo "<div class='campaign-carousel-text'>";
                                            the_title();
                                            echo "</div>";
                                            echo "</a>";
                                        endwhile;
                                	?>
                                </div>
                            </div>

                                	<!-- OLD VERSION -->
                                	<!--<div class="campaign-carousel">-->
                                  <?php

                                     /**   $args = array( 'post_type' => 'Campaigns');
                                        $loop= new WP_Query( $args );

                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            echo "<a href='".get_permalink()."'>";
                                            echo "<div class='campaign-carousel-text'>";
                                            the_title();
                                            echo "</div>";
                                        endwhile; **/
                                    ?>
                                	<!--</div>-->
                            
                            <!--END of Carousel-->








	</div>



</article><!-- #post-## -->




