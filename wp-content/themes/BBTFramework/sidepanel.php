
<div id="campaigns-next-main">
	<div id="campaigns-next-inner-main">
		MORE
	</div>
</div>

<div id="campaigns-menu">
	<div id="campaigns-next">
		<div id="campaigns-next-inner">
			<!--<div class="categories-more" id="categories-inner-more"><div class="categories-inner">MORE</div></div>-->
			<div class="categories-more" id="categories-inner-more"><div class="categories-inner">
				<img id="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue.png">
                <img id="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue.png">
			</div></div>
			<!--<div class="categories" id="categories-inner-flaunt"><div class="triangle" id="triangle-flaunt"></div><div class="categories-inner">SHOUT FLAUNT</div></div>
			<div class="categories" id="categories-inner-live"><div class="triangle" id="triangle-live"></div><div class="categories-inner">SHOUT LIVE</div></div>
			<div class="categories" id="categories-inner-convenience"><div class="triangle" id="triangle-convenience"></div><div class="categories-inner">CONVENIENCE</div></div>
			<div class="categories" id="categories-inner-street"><div class="triangle" id="triangle-street"></div><div class="categories-inner">STREET</div></div>
			<div class="categories" id="categories-inner-motion"><div class="triangle" id="triangle-motion"></div><div class="categories-inner">MOTION</div></div>-->
		</div>
	</div>

	<!-- campaign boxes -->

	<div id="campaign-boxes-main">

		<div id="getmore-more" class="get-more-boxes">
			<?php getMore(); ?>
		</div>

		<div id="getmore-live" class="get-more-boxes">
			<?php getMore('SHOUT LIVE'); ?>
		</div>

		<div id="getmore-connect" class="get-more-boxes">
			<?php getMore('SHOUT ACTIVATE'); ?>
		</div>

		<div id="getmore-motion" class="get-more-boxes">
			<?php getMore('MOTION'); ?>
		</div>

		<div id="getmore-convenience" class="get-more-boxes">
			<?php getMore('CONVENIENCE'); ?>
		</div>

		<div id="getmore-street" class="get-more-boxes">
			<?php getMore('STREET'); ?>
		</div>

		<div id="getmore-media" class="get-more-boxes">
			<?php getMore('CAFE MEDIA'); ?>
		</div>

		<div id="getmore-air" class="get-more-boxes">
			<?php getMore('AIR NEW ZEALAND'); ?>
		</div>

	</div>


	<!-- end of campaign boxes -->

</div>
</div>







<?php
function getMore($cat){

	?>
	<?php

	$args = array( 'post_type' => 'Campaigns', 'category_name'    => $cat);
	$loop = new WP_Query( $args );

	$count = count( get_posts( array( 
		'post_type' => 'Campaigns', 
							    'nopaging'  => true, // display all posts
							    ) ) );

	$count_posts = wp_count_posts('campaigns');
	$number_of_posts = $count_posts->publish; 

	$layer_count = 0;
	$mycount = 0;
	$campaign_number = 0;
	$layer = 0;

	if($cat == 'SHOUT LIVE'){
		$catname = "LIVE";
	}elseif ($cat == 'SHOUT CONNECT') {
		$catname = "CONNECT";
	}elseif ($cat == 'MOTION') {
		$catname = "MOTION";
	}elseif ($cat == 'CONVENIENCE') {
		$catname = "CONVENIENCE";
	}elseif ($cat == 'STREET') {
		$catname = "STREET";
	}elseif ($cat == 'CAFE MEDIA') {
		$catname = "MEDIA";
	}elseif ($cat == 'AIR NEW ZEALAND') {
		$catname = "AIR";
	}
	else{
		$catname = "MORE";
	}


	while ( $loop->have_posts() ) : $loop->the_post();

	if ($layer_count == 0){
		if($campaign_number == 0){
			echo "<div class='campaigns-boxes' id='campaign-boxes-".$campaign_number.$catname."' style='display:block'>";
		}
		else{
			echo "<div class='campaigns-boxes' id='campaign-boxes-".$campaign_number.$catname."' style='display:none'>";
		}
	}

	echo "<a href='".get_permalink()."'>";
	echo "<div class='campaigns-box'>";
	echo "<div class='campaigns-text'>";
	the_title();
	echo "</div>";
	echo "</div>";
	echo "</a>";	
	$mycount++;
	$layer_count++;

	if($mycount == 11){
		echo "<div class='campaigns-box-transparent'>";
		echo "<div class='campaigns-text'>";
		echo "<div class='cat-nav'>";

		if($campaign_number != 0){
			echo "<div class='cat-prev' id='".$campaign_number."' cat='".$catname."'><img class='arrows' src='/wp-content/uploads/2015/04/arrow_prev.png'></div>";
			$new_number_of_posts = $number_of_posts - (11 * $campaign_number);
			if ($new_number_of_posts > 11){
				if ($mycount == 11){
					echo "<div class='cat-next' id='".$campaign_number."' cat='".$catname."'><img class='arrows' src='/wp-content/uploads/2015/04/arrow_next.png'></div>";
				}
			}
		}
		else{
			if($number_of_posts > 11){
				echo "<div class='cat-next'><img class='arrows' src='/wp-content/uploads/2015/04/arrow_prev_no.png'></div>";
				echo "<div class='cat-next' id='".$campaign_number."' cat='".$catname."'><img class='arrows' src='/wp-content/uploads/2015/04/arrow_next.png'></div>";
			}
		}


								//echo $mycount;

		echo "</div>";
		echo "</div>";
		echo "</div>";

		echo "</div>";
		$mycount = 0;
		$campaign_number++;
		$layer_count = 0;
	}


	if($mycount == $number_of_posts){
		$blanks = 11 - $mycount;
		for ($i=0; $i < $blanks; $i++) { 
			echo "<div class='campaigns-box-blank'>";
			echo "<div class='campaigns-text'>";
			echo "</div>";
			echo "</div>";

		}

		echo "<div class='campaigns-box-blank'>";
		echo "<div class='campaigns-text'>";
								//echo "<div class='cat-nav'>";
								//if($campaign_number != 0){
								//	echo "<div class='cat-prev' id='".$campaign_number."'>PREV</div>";
								//}
								//echo "<div class='cat-next' id='".$campaign_number."'>NEXT</div>";
								//echo "</div>";
		echo "</div>";
		echo "</div>";


		echo "</div>";
		$mycount = 0;
		$layer_count = 0;	
		$stop = 0;	
		$campaign_number++;						
	}



	endwhile;

	$num = $mycount % 11;

	if($num != 0 && $number_of_posts > 11){
		$blanks = 11 - $mycount;
		for ($i=0; $i < $blanks; $i++) { 
			echo "<div class='campaigns-box-blank'>";
			echo "<div class='campaigns-text'>";
			echo "</div>";
			echo "</div>";

		}

		if($campaign_number != 0){
			echo "<div class='campaigns-box-transparent'>";
			echo "<div class='campaigns-text'>";
			echo "<div class='cat-nav'>";
			echo "<div class='cat-prev' id='".$campaign_number."' cat='".$catname."'><img class='arrows' src='/wp-content/uploads/2015/04/arrow_prev.png'></div>";
			echo "<div class='cat-prev'><img class='arrows' src='/wp-content/uploads/2015/04/arrow_next_no.png'></div>";
			echo "</div>";
		}else{
			echo "<div class='campaigns-box-blank'>";
			echo "<div class='campaigns-text'>";			
		}
								//echo "<div class='cat-next' id='".$campaign_number."'>NEXT</div>";

		echo "</div>";
		echo "</div>";

		echo "</div>";
		$layer_count = 0;	
		$campaign_number++;							
	}


	?>


	<?php
}
?>


<?php

