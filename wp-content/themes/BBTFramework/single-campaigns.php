<?php
/**
 * The Template for displaying all single posts.
 *
 * @package sparkling
 */

get_header(); ?>

  <div id="primary" class="content-area">

   
    <main id="main" class="site-main" role="main" style="height:100%">

						<?php //include('contact.php'); ?>

					<?php //include('sidepanel.php'); ?>

					<?php include('toppanel.php'); ?>
					

    				<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'campaigns', 'single' ); ?>

					<?php endwhile; // end of the loop. ?>

					<!--<div id="contact-desktop">-->

					<!--</div>-->




    </main><!-- #main -->
    


  </div><!-- #primary -->


<?php

?>

<?php get_footer(); ?>