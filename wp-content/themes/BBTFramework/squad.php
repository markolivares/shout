<?php
/**
 * Template Name: Squad
 *
 * This is the template for shout squad
 *
 * @package sparkling
 */

get_header(); ?>


  <div id="primary" class="content-area">

    <main id="main" class="site-main" role="main">

<div id="squad-main">
    
    <!--<div class="featured-image-squad">
        <?php the_post_thumbnail( 'sparkling-featured', array( 'class' => 'single-featured' )); ?>
    </div>


    <div id="first_person" class="squad_person">
        <div class="arrow-up-back"></div>
        <div class="arrow-up"></div>
        <div class="white"></div>
        <h1>JEN ROBINSON</h1>
        <h2>SALES DIRECTOR</h2>
        <div id="gettoknow_firstperson" class="gettoknow"><p><span id="span1">+</span> GET TO KNOW JEN</p></div>
    </div>


    <div id="second_person" class="squad_person">
        <div class="arrow-up-back"></div>
        <div class="arrow-up"></div>
        <h1>PAUL KENNY</h1>
        <h2>MANAGING DIRECTOR</h2>
        <div id="gettoknow_secondperson" class="gettoknow"><p><span id="span1">+</span> GET TO KNOW PAUL</p></div>
    </div>

    <div id="third_person" class="squad_person">
        <div class="arrow-up-back"></div>
        <div class="arrow-up"></div>
        <h1>PHIL WEEDON</h1>
        <h2>COMMERCIAL DIRECTOR</h2>
        <div id="gettoknow_thirdperson" class="gettoknow"><p><span id="span1">+</span> GET TO KNOW PHIL</p></div>
    </div>

    <div id="first_person_description" class="person_description">
        <h1>JEN ROBINSON</h1>
        <p>
        Fav band: Is Foreigner a band?<br/>
        Fav restaurant: Depot<br/>
        Fav movie: Die Hard and Clueless<br/>
        Fav drink: Margarita or Jelly Donut Shots or Cafe Patron or a great Shiraz<br/>
        If you were an animal what would you be? Why? Siberian Huskie – LOVE these dogs<br/>
        Jandels or shoes: Jandels in summer, shoes (boots) in winter<br/>
        Top of bucket list: Trek to Machu Picchu<br/>
        Watershed moment: Hearing the expression, “don’t do things the same if you want different results”.  I now live by this!<br/>
        </p>

        <div id="first_person_hide">- Hide</div>

    </div>

    <div id="third_person_description" class="person_description">
        <h1>PHIL WEEDON</h1>
        <p>
        Fav band: Metallica<br/>
        Fav restaurant: Mortons<br/>
        Fav movie: The Castle<br/>
        Fav drink: Whisky Sour<br/>
        If you were an animal what would you be? Why?  Rhino – horny & wrinkly<br/>
        Jandels or shoes: Jandels<br/>
        Top of bucket list: Glastonbury<br/>
        Watershed moment: We humans are 99% chimpanzee, stop being so hard on oneself!<br/>
        </p>

        <div id="third_person_hide">- Hide</div>

    </div>

    <div id="second_person_description" class="person_description">
        <h1>PAUL KENNY</h1>
        <p>
        Fav band: Ever - Stone Roses or Pixies and currently War on Drugs<br/>
        Fav restaurant: Currently Odettes however being a fickle Aucklander this will change in a couple of weeks<br/>
        Fav movie: Early De Niro - Taxi Driver, Apocalypse Now<br/>
        Fav drink: Latte in the morning and a Pinot Noir in the evening<br/>
        If you were an animal what would you be? Why? Floats like a butterfly, stings like a bee that’s why they call me Paul Kenny..<br/>
        Jandels or shoes: Depends on the season/occasion. Jandals don’t look great at a black tie function.<br/>
        Top of bucket list: To attend a FIFA World Cup<br/>
        Watershed moment: Professionally founding iSite Media - not being able to walk past a blank wall since then without wondering if I could put an advertisement on it.<br/>
        </p>

        <div id="second_person_hide">- Hide</div>

    </div>-->

<!-- New Squad Page -->

<div id="new-main-squad">
    <div class="new_squad">
        <div class="squad-box">
            <a href="/squad/paul-kenny/">
            <div class="squad-image"><img src="/wp-content/uploads/2015/07/paul.png"></div>
            <div class="squad-name">MEET PAUL</div>
            <div class="squad-position">MANAGING DIRECTOR</div>
            </a>
        </div>
    </div>
    <div class="new_squad">
        <div class="squad-box">
            <a href="/squad/jen-sergel/">
            <div class="squad-image"><img src="/wp-content/uploads/2015/07/jen.png"></div>
            <div class="squad-name">MEET JEN</div>
            <div class="squad-position">SALES DIRECTOR</div>
            </a>
        </div> 
    </div>
    <div class="new_squad">
        <div class="squad-box">
            <a href="/squad/phil-weedon/">
            <div class="squad-image"><img src="/wp-content/uploads/2015/07/phil.png"></div>
            <div class="squad-name">MEET PHIL</div>
            <div class="squad-position">COMMERCIAL DIRECTOR</div>
            </a>
        </div>
    </div>
    
    <div class="new_squad">
        <div class="squad-box">
            <a href="/squad/harry/">
            <div class="squad-image"><img src="/wp-content/uploads/2017/07/harry.jpg"></div>
            <div class="squad-name">MEET HARRY</div>
            <div class="squad-position">ACCOUNT MANAGER</div>
            </a>
        </div>
    </div>
    
    <div class="new_squad">
        <div class="squad-box">
            <a href="/squad/pete-stones/">
            <div class="squad-image"><img src="/wp-content/uploads/2016/02/pete.jpg"></div>
            <div class="squad-name">MEET PETE</div>
            <div class="squad-position">SITE DEVELOPMENT MANAGER</div>
            </a>
        </div>
    </div>

    <div class="new_squad">
        <div class="squad-box">
            <a href="/squad/samantha-brown/">
            <div class="squad-image"><img src="/wp-content/uploads/2017/10/Sam-squad.jpg"></div>
            <div class="squad-name">MEET SAM</div>
            <div class="squad-position">PRODUCTION &amp; CONTENT MANAGER</div>
            </a>
        </div>
    </div>

    <div class="new_squad">
        <div class="squad-box">
            <a href="/squad/rebecca/">
            <div class="squad-image"><img src="/wp-content/uploads/2017/10/Rebecca-squad.jpg"></div>
            <div class="squad-name">MEET REBECCA</div>
            <div class="squad-position">SALES &amp; CLIENT LIAISON</div>
            </a>
        </div>
    </div>

</div>

</div>

 <!-- END OF SQUAD MAIN -->


<div id="squad-main-small">

    <div class="squad-box">
        <a href="/squad/paul-kenny/">
        <div class="squad-image"><img src="/wp-content/uploads/2015/07/paul.png"></div>
        <div class="squad-name">MEET PAUL</div>
        <div class="squad-position">MANAGING DIRECTOR</div>
        </a>
    </div>

    <div class="squad-box">
        <a href="/squad/jen-sergel/">
        <div class="squad-image"><img src="/wp-content/uploads/2015/07/jen.png"></div>
        <div class="squad-name">MEET JEN</div>
        <div class="squad-position">SALES DIRECTOR</div>
        </a>
    </div>     

    <div class="squad-box">
        <a href="/squad/phil-weedon/">
        <div class="squad-image"><img src="/wp-content/uploads/2015/07/phil.png"></div>
        <div class="squad-name">MEET PHIL</div>
        <div class="squad-position">COMMERCIAL DIRECTOR</div>
        </a>
    </div>   
    

    <div class="squad-box">
            <a href="/squad/harry/">
            <div class="squad-image"><img src="z/wp-content/uploads/2017/07/harry.jpg"></div>
            <div class="squad-name">MEET HARRY</div>
            <div class="squad-position">ACCOUNT MANAGER</div>
            </a>
        </div>

    <div class="squad-box">
            <a href="/squad/pete-stones/">
            <div class="squad-image"><img src="/wp-content/uploads/2016/02/pete.jpg"></div>
            <div class="squad-name">MEET PETE</div>
            <div class="squad-position">SITE DEVELOPMENT MANAGER</div>
            </a>
        </div>
</div>

    		<?php //include('contact.php'); ?>

    </main><!-- #main -->

  </div><!-- #primary -->

<?php get_footer(); ?>