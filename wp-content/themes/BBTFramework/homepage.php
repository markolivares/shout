<?php
/**
 * Template Name: Homepage
 *
 * This is the template for homepage
 *
 * @package sparkling
 */

get_header(); ?>

  <div id="primary" class="content-area">

    <main id="main" class="site-main" role="main" style="height: auto">

    				<?php include('sidepanel.php'); ?>

    				<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'homepage', 'single' ); ?>

					<?php endwhile; // end of the loop. ?>


    </main><!-- #main -->


  </div><!-- #primary -->


<?php

?>

<?php get_footer(); ?>