<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package sparkling
 */
?>
</a>

<?php
  if(is_singular( 'campaigns' )){
    echo "</a>";

    include('contact.php');

  }

  if (is_front_page()) {
    include('contact.php');
  }
?>

</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->

<?php
  //if(!is_singular( 'campaigns' )){

    include('contact.php');
    
  //}
?>


</div><!-- close .row -->

</div> <!-- super main -->
</div><!-- close .container -->

 

</div><!-- close .site-content -->

<div id="footer-area">

		<!--<div class="container footer-inner">
			<div class="row">
				<?php get_sidebar( 'footer' ); ?>
			</div>
		</div>-->

        <div id="small-footer"> 
            <!--<div id="bottom-menu">
                 <div class="navigation-small">
                    <div id="prev-small"><?php previous_post('%','<img class="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue_prev.png">', 'no'); ?> </div>
                    <div id="small-menu"><img id="small-menu-image" src="/wp-content/uploads/2015/11/small-menu.png"></div>
                    <div id="next-small"><?php next_post('%','<img class="blue-arrow" src="/wp-content/uploads/2015/11/arrow_blue_next.png">', 'no'); ?></div>
               </div>
            </div>-->
            <div id="small-footer-inner">
                <div id="small-footer-inner-text">
                    LATEST AND GREATEST FROM THE TEAM AT SHOUT
                </div>
                    <div id="megaphone-small">
                        <a id="megaphone-link"><img id="megaphone-image" src="/wp-content/uploads/2015/04/megaphone-1.png" onmouseover="this.src='/wp-content/uploads/2015/11/megaphone-2.png'" 
                            onmouseout="this.src='/wp-content/uploads/2015/04/megaphone-1.png'"/></a>
                    </div>
                    <div id="megaphone-close-small">
                        <a id="megaphone-link"><img id="megaphone-image" src="/wp-content/uploads/2015/04/megaphone-1.png" onmouseover="this.src='/wp-content/uploads/2015/11/megaphone-2.png'" 
                            onmouseout="this.src='/wp-content/uploads/2015/04/megaphone-1.png'"/></a>
                    </div>

            </div>
        </div>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info container">
				<div class="row" style="position: relative">

					<div id="footer-text">
						LATEST AND GREATEST FROM THE TEAM AT SHOUT
					</div>

					<div id="megaphone" style="display: block;width: 155px;height: 170px;">
						<a id="megaphone-link" ><img id="megaphone-image" src="/wp-content/uploads/2015/04/megaphone-1.png" onmouseover="this.src='/wp-content/uploads/2015/11/megaphone-2.png'" 
                            onmouseout="this.src='/wp-content/uploads/2015/04/megaphone-1.png'"/></a>
                    </div>
                    <div id="megaphone-close" style="display: block;width: 155px;height: 170px;">
                        <a id="megaphone-link"><img id="megaphone-image" src="/wp-content/uploads/2015/04/megaphone-1.png" onmouseover="this.src='/wp-content/uploads/2015/11/megaphone-2.png'" 
                            onmouseout="this.src='/wp-content/uploads/2015/04/megaphone-1.png'"/></a>
                    </div>

					<!--
					<?php sparkling_social(); ?>
					<nav role="navigation" class="col-md-6">
						<?php sparkling_footer_links(); ?>
					</nav>
					<div class="copyright col-md-6">
						<?//php echo of_get_option( 'custom_footer_text', 'sparkling' ); ?>
						<?//php sparkling_footer_info(); ?>
					</div>-->
				</div>
			</div><!-- .site-info -->
			<!--<div class="scroll-to-top"><i class="fa fa-angle-up"></i></div>--><!-- .scroll-to-top -->
		</footer><!-- #colophon -->






	</div>
</div><!-- #page -->



<?php wp_footer(); ?>

    <script>
        $(document).ready(function(){
          $('.campaign-carousel').slick({
              centerMode: true,
              centerPadding: '60px',
              slidesToShow: 3,
              responsive: [
                {
                  breakpoint: 384,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 3
                  }
                },
                {
                  breakpoint: 240,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 1
                  }
                }
              ]
            });
        });

    </script>

<script>
$(document).ready(function(){

    $("#megaphone").click(function(){

        //$("#overlay-black").fadeIn(400);
        $("#inner-layer-black").fadeIn(400);
        $("#inner-layer").fadeIn(400);

         /**$("#contact-layer").css({'overflow' : 'hidden'});
         $("#contact-layer").css({'z-index' : '200'});
         $("#contact-layer").css({'transition' : 'opacity 1s'});
         $("#contact-layer").css({'opacity' : '1'});
         
         $("#contact-layer-black").css({'overflow' : 'hidden'});
         $("#contact-layer-black").css({'z-index' : '100'});
         $("#contact-layer-black").css({'transition' : 'opacity 1s'});
         $("#contact-layer-black").css({'opacity' : '0.75'});

         $("#contact-layer-black-next").css({'overflow' : 'hidden'});
         $("#contact-layer-black-next").css({'transition' : 'opacity 1s'});
         $("#contact-layer-black-next").css({'opacity' : '0'}); 
         $("#contact-layer-black-next").css({'z-index' : '-100'}); 
         
         $("#campaigns-menu").css({'z-index' : '0'});**/
    

        $("#megaphone").css({'display' : 'none'});
         $("#megaphone-close").css({'display' : 'block'});

         //$("#campaigns-next-main").css({'z-index' : '0'});
         //$("#media-next-main").css({'z-index' : '0'});

         //$("#campaigns-menu").css({'z-index' : '0'});
         //$("#media-menu").css({'z-index' : '0'});
    
         closeMedia();
         closeCampaign();


     });

$("#megaphone-small").click(function(){
    //$("#overlay-black").fadeIn(400);
    $("#inner-layer-black").fadeIn(400);
    $("#inner-layer").fadeIn(400);
    $("#megaphone-small").css({'display' : 'none'});
    $("#megaphone-close-small").css({'display' : 'block'});
    hideSmallCampaigns();

    closeSecondaryMenu();

});

$("#menu-item-228").click(function(){
    //$("#overlay-black").fadeIn(400);
    $("#inner-layer-black").fadeIn(400);
    $("#inner-layer").fadeIn(400);
    $("#megaphone-small").css({'display' : 'none'});
    $("#megaphone-close-small").css({'display' : 'block'});
    hideSmallCampaigns();

    closeSecondaryMenu();

});

$("#megaphone-close-small").click(function(){
    //$("#overlay-black").fadeOut(400);
    $("#inner-layer-black").fadeOut(400);
    $("#inner-layer").fadeOut(400);
    $("#megaphone-small").css({'display' : 'block'});
    $("#megaphone-close-small").css({'display' : 'none'});
    hideSmallCampaigns();
    //closeSecondaryMenu();
    closeSecondaryMenu();
});

$("#small-menu").click(function(){
  //$("#overlay-black").fadeIn(400);
  $("#inner-layer").fadeOut(400);
  $("#inner-layer-black").fadeIn(400);
  showSmallCampaigns();
  //closeSecondaryMenu();
  closeSecondaryMenu();
});


$("#small-menu-2").click(function(){
  //$("#overlay-black").fadeOut(400);
  $("#inner-layer").fadeOut(400);
  $("#inner-layer-black").fadeOut(400);
  hideSmallCampaigns();
  closeSecondaryMenu();
});


$("#top-small-menu").click(function(){
  //$("#overlay-black").fadeIn(400);
  $("#inner-layer").fadeOut(400);
  $("#inner-layer-black").fadeIn(400);
  showSmallCampaignsTop();
  //closeSecondaryMenu();
  closeSecondaryMenu();
});

$("#top-small-menu-2").click(function(){
  //$("#overlay-black").fadeOut(400);
  $("#inner-layer").fadeOut(400);
  $("#inner-layer-black").fadeOut(400);
  hideSmallCampaignsTop();
  closeSecondaryMenu();
});

function showSmallCampaigns(){
    $("#campaign-carousel-main").css({'z-index' : '10'});
    $("#small-menu-2").css({'display' : 'block'});
    $("#small-menu").css({'display' : 'none'});
}

function hideSmallCampaigns(){
   $("#campaign-carousel-main").css({'z-index' : '-10'});
    $("#small-menu-2").css({'display' : 'none'});
    $("#small-menu").css({'display' : 'block'}); 
}

function showSmallCampaignsTop(){
    $("#campaign-carousel-main").css({'z-index' : '10'});
    $("#top-small-menu-2").css({'display' : 'block'});
    $("#top-small-menu").css({'display' : 'none'});
}

function hideSmallCampaignsTop(){
   $("#campaign-carousel-main").css({'z-index' : '-10'});
    $("#top-small-menu-2").css({'display' : 'none'});
    $("#top-small-menu").css({'display' : 'block'}); 
}



/** Closing the contact form **/

$("#contact-layer").click(function(){
    closeContact();
         $("#megaphone").css({'display' : 'block'});
         $("#megaphone-close").css({'display' : 'none'});
   });

$("#megaphone-close").click(function(){
    //closeContact();
    //$("#overlay-black").fadeOut(400);
    $("#inner-layer-black").fadeOut(400);
    $("#inner-layer").fadeOut(400);
         $("#megaphone").css({'display' : 'block'});
         $("#megaphone-close").css({'display' : 'none'});
        closeMedia();
        closeCampaign();

});

//$("#overlay-black").click(function(){
$("#inner-layer").click(function(){
    //closeContact();
    //$("#overlay-black").fadeOut(400);
    $("#inner-layer-black").fadeOut(400);
    $("#inner-layer").fadeOut(400);
         $("#megaphone").css({'display' : 'block'});
         $("#megaphone-close").css({'display' : 'none'});
            if($( window ).width() > 996){
              closeMedia();
              closeCampaign();
            }
        //closeSecondaryMenu();

});

$("#inner-layer-black").click(function(){
    //closeContact();
    //$("#overlay-black").fadeOut(400);
    $("#inner-layer-black").fadeOut(400);
    $("#inner-layer").fadeOut(400);
         $("#megaphone").css({'display' : 'block'});
         $("#megaphone-close").css({'display' : 'none'});
            if($( window ).width() > 996){
              closeMedia();
              closeCampaign();
            }
            closeSecondaryMenu();
            hideSmallCampaigns();

});

function closeContact(){

      $("#contact-layer").css({'overflow' : 'hidden'});
       $("#contact-layer").css({'transition' : 'opacity 1s'});
       $("#contact-layer").css({'opacity' : '0'});

       $("#contact-layer-black").css({'overflow' : 'hidden'});
       $("#contact-layer-black").css({'transition' : 'opacity 1s'});
       $("#contact-layer-black").css({'opacity' : '0'});         

       setTimeout(function() {
        contact_index();
    }, 1000); 
}


function contact_index(){
   $("#contact-layer").css({'z-index' : '-100'});
   $("#contact-layer-black").css({'z-index' : '-100'});
}

    /**$("#campaigns-next").click(function(){
    	$('#campaigns-boxes').show(2000);
    	$("#campaigns-menu").css({'z-index' : '200'});

    	 $("#contact-layer-black-next").css({'overflow' : 'hidden'});
         $("#contact-layer-black-next").css({'z-index' : '0'});
         $("#contact-layer-black-next").css({'transition' : 'opacity 1s'});
         $("#contact-layer-black-next").css({'opacity' : '0.75'});

     });**/


$('.campaigns-box').hover(
   function () {
     $(this).css({"background-color":"#009FDA"});
 }, 
 function () {
     $(this).css({"background-color":"#ff0099"});
 }
 );

<?php
if (is_page('shout-squad')){
   ?>
   showSquad();
   <?php
}
?>

function showSquad(){
    $('#first_person').fadeIn(2000);
    $('#first_person').css({'left' : '12%'});
    $('#first_person').css({'transition' : 'left 1s'});
    setTimeout(function() {
     $('#third_person').fadeIn(2000);
     $('#third_person').css({'right' : '42%'});
     $('#third_person').css({'transition' : 'right 1s'});
 }, 500);
    setTimeout(function() {
     $('#second_person').fadeIn(2000);
     $('#second_person').css({'right' : '17%'});
     $('#second_person').css({'transition' : 'right 1s'});
 }, 1000);
}

function hideSquad(){
    $('#first_person').fadeOut(1000);
    $('#first_person').css({'left' : '10%'});
    $('#first_person').css({'transition' : 'left 2s'});
    setTimeout(function() {
     $('#third_person').fadeOut(1000);
     $('#third_person').css({'right' : '46%'});
     $('#third_person').css({'transition' : 'right 2s'});
 }, 500);
    setTimeout(function() {
     $('#second_person').fadeOut(1000);
     $('#second_person').css({'right' : '24%'});
     $('#second_person').css({'transition' : 'right 2s'});
 }, 1000);
}

/** Hover **/

$( "#first_person" ).mouseover(function() {
  $( "#first_person" ).css({'background-color' : '#ff0099'});
  $( "#first_person .arrow-up" ).css({'border-bottom' : '25px solid #ff0099'});
  $( "#first_person #span1" ).css({'color' : '#009FDA'});
});

$( "#first_person" ).mouseout(function() {
  $( "#first_person" ).css({'background-color' : '#009FDA'});
  $( "#first_person .arrow-up" ).css({'border-bottom' : '25px solid #009FDA'});
  $( "#first_person #span1" ).css({'color' : '#ff0099'});
});

$( "#second_person" ).mouseover(function() {
  $( "#second_person" ).css({'background-color' : '#ff0099'});
  $( "#second_person .arrow-up" ).css({'border-bottom' : '25px solid #ff0099'});
  $( "#second_person #span1" ).css({'color' : '#009FDA'});
});

$( "#second_person" ).mouseout(function() {
  $( "#second_person" ).css({'background-color' : '#009FDA'});
  $( "#second_person .arrow-up" ).css({'border-bottom' : '25px solid #009FDA'});
  $( "#second_person #span1" ).css({'color' : '#ff0099'});
});

$( "#third_person" ).mouseover(function() {
  $( "#third_person" ).css({'background-color' : '#ff0099'});
  $( "#third_person .arrow-up" ).css({'border-bottom' : '25px solid #ff0099'});
  $( "#third_person #span1" ).css({'color' : '#009FDA'});
});

$( "#third_person" ).mouseout(function() {
  $( "#third_person" ).css({'background-color' : '#009FDA'});
  $( "#third_person .arrow-up" ).css({'border-bottom' : '25px solid #009FDA'});
  $( "#third_person #span1" ).css({'color' : '#ff0099'});
});


/** First Person **/

$('#first_person').click(function(){
    hideSquad();
    setTimeout(function() {
        showFirstPerson();
    }, 1000);
});

function showFirstPerson(){
    $('#first_person_description').fadeIn(1000);
}

$('#first_person_hide').click(function(){
    hideFirstPerson();
    setTimeout(function() {
        showSquad();
    }, 500);
});

function hideFirstPerson(){
    $('#first_person_description').fadeOut(1000);
}

/** Second Person **/

$('#second_person').click(function(){
    hideSquad();
    setTimeout(function() {
        showSecondPerson();
    }, 1000);
});

function showSecondPerson(){
    $('#second_person_description').fadeIn(1000);
}

$('#second_person_hide').click(function(){
    hideSecondPerson();
    setTimeout(function() {
        showSquad();
    }, 500);
});

function hideSecondPerson(){
    $('#second_person_description').fadeOut(1000);
}


/** Third Person **/

$('#third_person').click(function(){
    hideSquad();
    setTimeout(function() {
        showThirdPerson();
    }, 1000);
});

function showThirdPerson(){
    $('#third_person_description').fadeIn(1000);
}

$('#third_person_hide').click(function(){
    hideThirdPerson();
    setTimeout(function() {
        showSquad();
    }, 500);
});

function hideThirdPerson(){
    $('#third_person_description').fadeOut(1000);
}

/** Campaigns **/

/** Open Campaign **/


/** OPEN FROM CATEGORIES **/

$('#category-name').click(function(){
    var this_category = $(this).text();
    openCampaign(this_category);
});

$('#campaigns-next-main').click(function(){
    openCampaign();
});

function openCampaign(category){

    //alert(category);

    if (category == 'SHOUT LIVE'){
        displayLive();
    }else if(category == 'SHOUT ACTIVATE'){
        displayConnect();
    }else if(category == 'MOTION'){
        displayMotion();
    }else if(category == 'CONVENIENCE'){
        displayConvenience();
    }else if(category == 'STREET POSTER'){
        displayStreet();
    }else if(category == 'CAFE MEDIA'){
        displayMedia();
    }else if(category == 'AIR NEW ZEALAND'){
        displayAir();
    }else{
        displayMore();
    }

    /**$('#campaigns-menu').css({'z-index' : '100'});
    $("#inner-layer-black").fadeIn(400);
    
    $("#megaphone").css({'display' : 'none'});
    $("#megaphone-close").css({'display' : 'block'});

   

    $('#campaigns-next-main').css({'right' : '-5px'});
    $('#campaigns-next-main').css({'transition' : 'right 0.3s'});
    setTimeout(function() {
        $('#campaigns-next-inner-main').fadeOut(300);
    }, 100);

    setTimeout(function() {
        $('#campaigns-menu').show('slide',{direction:'right'},1000);
        setTimeout(function() {
            $('#campaigns-next-main').hide();
                $('#campaigns-next-inner').fadeIn(300);
        }, 300);
    }, 100);**/

    showTopCampaign();

}

/** Close Campaign **/

$('#contact-layer-black-next').click(function(){
    closeMedia();
    closeCampaign();
});

function closeCampaign(){
    $('#campaigns-menu').hide('slide',{direction:'right'},1000);
    setTimeout(function() {
        $('#campaigns-next-main').show();
        $('#campaigns-next-inner').fadeOut(300);
    }, 300);
    setTimeout(function() {
        $('#campaigns-next-main').css({'right' : '0px'});
        $('#campaigns-next-main').css({'transition' : 'right 0.3s'});
        setTimeout(function() {
            $('#campaigns-next-inner-main').fadeIn(300);
        }, 100);
    }, 800);

    setTimeout(function() {

        $("#contact-layer-black-next").css({'overflow' : 'hidden'});
        $("#contact-layer-black-next").css({'transition' : 'opacity 1s'});
        $("#contact-layer-black-next").css({'opacity' : '0'}); 
        $("#contact-layer-black-next").css({'z-index' : '-100'});
    }, 800);

}


/** Media **/

/** Open Media **/
$('#media-next-main').click(function(){
    /**$("#contact-layer-black-next").css({'overflow' : 'hidden'});
    $("#contact-layer-black-next").css({'z-index' : '0'});
    $("#contact-layer-black-next").css({'transition' : 'opacity 1s'});
    $("#contact-layer-black-next").css({'opacity' : '0.75'});**/

    $("#media-menu").css({'z-index' : '10'});

    //$("#overlay-black").fadeIn(400);
    $("#inner-layer-black").fadeIn(400);
        
        $("#megaphone").css({'display' : 'none'});
         $("#megaphone-close").css({'display' : 'block'});

    $('#media-next-main').css({'right' : '-5px'});
    $('#media-next-main').css({'transition' : 'right 0.3s'});
    setTimeout(function() {
        $('#media-next-inner-main').fadeOut(300);
    }, 100);

    setTimeout(function() {
        $('#media-menu').show('slide',{direction:'right'},1000);
        setTimeout(function() {
            $('#media-next-main').hide();
            $('#media-next-inner').fadeIn(300);
        }, 300);
    }, 100);
});


/** Close Media **/

$('#media-next').click(function(){
    closeMedia();
        //$("#overlay-black").fadeOut(400);
         $("#inner-layer-black").fadeOut(400);
         $("#megaphone").css({'display' : 'block'});
         $("#megaphone-close").css({'display' : 'none'});
});

function closeMedia(){
    $("#media-menu").css({'z-index' : '10'});
    $('#media-menu').hide('slide',{direction:'right'},1000);
    setTimeout(function() {
        $('#media-next-main').show();
        $('#media-next-inner').fadeOut(300);
    }, 300);
    setTimeout(function() {
        $('#media-next-main').css({'right' : '0px'});
        $('#media-next-main').css({'transition' : 'right 0.3s'});
        setTimeout(function() {
            $('#media-next-inner-main').fadeIn(300);
        }, 100);
    }, 800);

    setTimeout(function() {

        /**$("#contact-layer-black-next").css({'overflow' : 'hidden'});
        $("#contact-layer-black-next").css({'transition' : 'opacity 1s'});
        $("#contact-layer-black-next").css({'opacity' : '0'}); 
        $("#contact-layer-black-next").css({'z-index' : '-100'});**/
    }, 800);

}

});

/** NEXT Categories **/
$('.cat-next').click(function(){
    var thisID = $(this).attr('id');
    var nextID = parseInt($(this).attr('id')) + 1;
    var cat = $(this).attr('cat');
    OpenNext(thisID, nextID, cat);
});

function OpenNext(idd, iddnext, cat){
    $("#campaign-boxes-"+idd+cat).css({'display' : 'none'});
    $("#campaign-boxes-"+iddnext+cat).css({'display' : 'block'});
}


/** PREV Categories **/
$('.cat-prev').click(function(){
    var thisID = $(this).attr('id');
    var prevID = parseInt($(this).attr('id')) - 1;
    var cat = $(this).attr('cat');
    OpenPrev(thisID, prevID, cat);
});

function OpenPrev(idd, iddprev, cat){
    $("#campaign-boxes-"+idd+cat).css({'display' : 'none'});
    $("#campaign-boxes-"+iddprev+cat).css({'display' : 'block'});
}

/** CHANGE CATEGORY **/
$('#categories-inner-more').click(function(){
    //displayMore();
    closeCampaign();   
});

function closeCampaign(){
    $('#campaigns-menu').hide('slide',{direction:'right'},1000);
    setTimeout(function() {
        $('#campaigns-next-main').show();
        $('#campaigns-next-inner').fadeOut(300);
    }, 300);
    setTimeout(function() {
        $('#campaigns-next-main').css({'right' : '0px'});
        $('#campaigns-next-main').css({'transition' : 'right 0.3s'});
        setTimeout(function() {
            $('#campaigns-next-inner-main').fadeIn(300);
        }, 100);
    }, 800);

    setTimeout(function() {

        $('#campaigns-menu').css({'z-index' : '0'});
        //$("#overlay-black").fadeOut(400);
        $("#inner-layer-black").fadeOut(400);
        $("#megaphone").css({'display' : 'block'});
        $("#megaphone-close").css({'display' : 'none'});

        /**$("#contact-layer-black-next").css({'overflow' : 'hidden'});
        $("#contact-layer-black-next").css({'transition' : 'opacity 1s'});
        $("#contact-layer-black-next").css({'opacity' : '0'}); 
        $("#contact-layer-black-next").css({'z-index' : '-100'});**/
    }, 800);

}

/**
$('#categories-inner-flaunt').click(function(){
    displayFlaunt();
});

$('#categories-inner-live').click(function(){
    displayLive();
});

$('#categories-inner-convenience').click(function(){
    displayConvenience();
});


$('#categories-inner-street').click(function(){
    displayStreet();
});

$('#categories-inner-motion').click(function(){
    displayMotion();
});
**/

function displayMore(){
    $('#getmore-more').css({'display' : 'block'});
    $('#getmore-live').css({'display' : 'none'});
    $('#getmore-connect').css({'display' : 'none'});
    $('#getmore-motion').css({'display' : 'none'});
    $('#getmore-convenience').css({'display' : 'none'});
    $('#getmore-street').css({'display' : 'none'});
    $('#getmore-media').css({'display' : 'none'});
    $('#getmore-air').css({'display' : 'none'});

    /**$('#getmore-more').css({'display' : 'block'});
    $('#getmore-flaunt').css({'display' : 'none'});
    $('#getmore-live').css({'display' : 'none'});
    $('#getmore-convenience').css({'display' : 'none'});
    $('#getmore-street').css({'display' : 'none'});
    $('#getmore-motion').css({'display' : 'none'});

    $('#triangle-flaunt').css({'display' : 'none'});
    $('#triangle-live').css({'display' : 'none'});
    $('#triangle-convenience').css({'display' : 'none'});
    $('#triangle-street').css({'display' : 'none'});
    $('#triangle-motion').css({'display' : 'none'}); **/
}

function displayLive(){
    $('#getmore-more').css({'display' : 'none'});
    $('#getmore-live').css({'display' : 'block'});
    $('#getmore-connect').css({'display' : 'none'});
    $('#getmore-motion').css({'display' : 'none'});
    $('#getmore-convenience').css({'display' : 'none'});
    $('#getmore-street').css({'display' : 'none'});
    $('#getmore-media').css({'display' : 'none'});
    $('#getmore-air').css({'display' : 'none'});
}


function displayConnect(){
    $('#getmore-more').css({'display' : 'none'});
    $('#getmore-live').css({'display' : 'none'});
    $('#getmore-connect').css({'display' : 'block'});
    $('#getmore-motion').css({'display' : 'none'});
    $('#getmore-convenience').css({'display' : 'none'});
    $('#getmore-street').css({'display' : 'none'});
    $('#getmore-media').css({'display' : 'none'});
    $('#getmore-air').css({'display' : 'none'});
}

function displayMotion(){
    $('#getmore-more').css({'display' : 'none'});
    $('#getmore-live').css({'display' : 'none'});
    $('#getmore-connect').css({'display' : 'none'});
    $('#getmore-motion').css({'display' : 'block'});
    $('#getmore-convenience').css({'display' : 'none'});
    $('#getmore-street').css({'display' : 'none'});
    $('#getmore-media').css({'display' : 'none'});
    $('#getmore-air').css({'display' : 'none'});
}

function displayConvenience(){
    $('#getmore-more').css({'display' : 'none'});
    $('#getmore-live').css({'display' : 'none'});
    $('#getmore-connect').css({'display' : 'none'});
    $('#getmore-motion').css({'display' : 'none'});
    $('#getmore-convenience').css({'display' : 'block'});
    $('#getmore-street').css({'display' : 'none'});
    $('#getmore-media').css({'display' : 'none'});
    $('#getmore-air').css({'display' : 'none'});
}

function displayStreet(){
    $('#getmore-more').css({'display' : 'none'});
    $('#getmore-live').css({'display' : 'none'});
    $('#getmore-connect').css({'display' : 'none'});
    $('#getmore-motion').css({'display' : 'none'});
    $('#getmore-convenience').css({'display' : 'none'});
    $('#getmore-street').css({'display' : 'block'});
    $('#getmore-media').css({'display' : 'none'});
    $('#getmore-air').css({'display' : 'none'});
}

function displayMedia(){
    $('#getmore-more').css({'display' : 'none'});
    $('#getmore-live').css({'display' : 'none'});
    $('#getmore-connect').css({'display' : 'none'});
    $('#getmore-motion').css({'display' : 'none'});
    $('#getmore-convenience').css({'display' : 'none'});
    $('#getmore-street').css({'display' : 'none'});
    $('#getmore-media').css({'display' : 'block'});
    $('#getmore-air').css({'display' : 'none'});
}

function displayAir(){
    $('#getmore-more').css({'display' : 'none'});
    $('#getmore-live').css({'display' : 'none'});
    $('#getmore-connect').css({'display' : 'none'});
    $('#getmore-motion').css({'display' : 'none'});
    $('#getmore-convenience').css({'display' : 'none'});
    $('#getmore-street').css({'display' : 'none'});
    $('#getmore-media').css({'display' : 'none'});
    $('#getmore-air').css({'display' : 'block'});
}

function showBlack(){
    $("#contact-layer-black-next").css({'overflow' : 'hidden'});
    $("#contact-layer-black-next").css({'z-index' : '0'});
    $("#contact-layer-black-next").css({'transition' : 'opacity 1s'});
    $("#contact-layer-black-next").css({'opacity' : '0.75'});
}

function hideBlack(){
    $("#contact-layer-black-next").css({'overflow' : 'hidden'});
    $("#contact-layer-black-next").css({'transition' : 'opacity 1s'});
    $("#contact-layer-black-next").css({'opacity' : '0'}); 
    $("#contact-layer-black-next").css({'z-index' : '-100'});    
}

/** RESPONSIVENESS PORTRAIT **/

function showSmallCampaigns(){
    $("#campaign-carousel-main").css({'z-index' : '10'});
    $("#small-menu-2").css({'display' : 'block'});
    $("#small-menu").css({'display' : 'none'});
}

function hideSmallCampaigns(){
   $("#campaign-carousel-main").css({'z-index' : '-10'});
    $("#small-menu-2").css({'display' : 'none'});
    $("#small-menu").css({'display' : 'block'}); 
}

$('#arrow-down-menu').click(function(){
    //$("#overlay-black").fadeIn(400);
    $("#inner-layer-black").fadeIn(400);
    $("#inner-layer").fadeOut(400);
    openSecondaryMenu();
    hideSmallCampaigns();
});

$('#arrow-down-menu-2').click(function(){
    //$("#overlay-black").fadeOut(400);
    $("#inner-layer-black").fadeOut(400);
    closeSecondaryMenu();
    hideSmallCampaigns();
});

function openSecondaryMenu(){
    $('#secondary-menu-li').slideDown();
    $('#arrow-down-menu-2').css({'display' : 'block'});
    $('#arrow-down-menu').css({'display' : 'none'});
    $('#sec-menu').css({'display' : 'block'});
    $('#sec-title').css({'display' : 'none'});
}

function closeSecondaryMenu(){
    $('#secondary-menu-li').slideUp();
    $('#arrow-down-menu').css({'display' : 'block'});
    $('#arrow-down-menu-2').css({'display' : 'none'});
    $('#sec-title').css({'display' : 'block'});
    $('#sec-menu').css({'display' : 'none'});
}

$("#menu-item-228").click(function(){
    $("#inner-layer-black").fadeIn(400);
    $("#inner-layer").fadeIn(400);
    $("#megaphone-small").css({'display' : 'none'});
    $("#megaphone-close-small").css({'display' : 'block'});
    hideSmallCampaigns();
    closeSecondaryMenu();
});


$("img#prevnext-center-image").click(function(){
  displayMore();
  showTopCampaign();
});

function showTopCampaign(){
    $("#topmenu").slideDown(600);
    $("#prevnext-center-image-close").css("display","block");
    $("#prevnext-center-image").css("display","none");
}

$("img#prevnext-center-image-close").click(function(){
    $("#topmenu").slideUp(600);
    $("#prevnext-center-image-close").css("display","none");
    $("#prevnext-center-image").css("display","block");
});

//$("#topmenu").append($("#top-campaigns-menu").html());

$("#top-campaigns-menu").detach().appendTo('#topmenu');

//$("#top-campaigns-menu").html();



</script>

</body>
</html>