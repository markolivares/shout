<div id="campaigns-menu">
    				<div id="campaigns-next">
    					<div id="campaigns-next-inner">
    						MORE
    					</div>
    				</div>
    				<div id="campaigns-boxes">
    					  <?php
						  $args = array( 'post_type' => 'Campaigns');
						$loop = new WP_Query( $args );

						$count = count( get_posts( array( 
							    'post_type' => 'Campaigns', 
							    'nopaging'  => true, // display all posts
							) ) );

						$number_in_rows = 3;

						$start = 0;
						$mycount = 0;
						while ( $loop->have_posts() ) : $loop->the_post();

							if($start == 0){
								echo "<div class='campaigns-box-row'>";

							}

							echo "<a href='".get_permalink()."'>";
							echo "<div class='campaigns-box'>";
							echo "<div class='campaigns-text'>";
							the_title();
							echo "</div>";
							echo "</div>";
							echo "</a>";	
							$start ++;
							$mycount++;

							if($count == $mycount){
								$remaining = $number_in_rows - ($mycount % $number_in_rows);
								if ($remaining != $number_in_rows){
									for ($i=0; $i < $remaining; $i++) { 
										echo "<div class='campaigns-box-blank'>";
										echo "<div class='campaigns-text'>";
										//echo "YES";
										echo "</div>";
										echo "</div>";
									}
								}
							}

							if($start == $number_in_rows){
								echo "</div>";
								$start = 0;
							}

							
						endwhile;
						  ?>
    				</div>
    			</div>