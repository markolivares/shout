<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package sparkling
 */
?>

<div class="media_page">
<div class="post-inner-content">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div id="main_media">
	<div id="main_media_image">
	        <?php the_post_thumbnail( 'full', array( 'class' => 'single-featured' )); ?>
	 </div>
	 <div class="main_media_text">

	 	<h1 class="entry-title"><?php the_title(); ?></h1>

	 		<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'sparkling' ),
				'after'  => '</div>',
			) );
		?>
    <?php
      // Checks if this is homepage to enable homepage widgets
      if ( is_front_page() ) :
        get_sidebar( 'home' );
      endif;
    ?>
	</div><!-- .entry-content -->
	 </div>
</div>

<div id="main_media_small">


	<div id="media-featured-image">
		<?php the_post_thumbnail( 'full', array( 'class' => 'single-featured' )); ?>
	</div>


	<div id="media-featured-title">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</div>


	<div id="media-featured-content">
			<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'sparkling' ),
				'after'  => '</div>',
			) );
		?>
    <?php
      // Checks if this is homepage to enable homepage widgets
      if ( is_front_page() ) :
        get_sidebar( 'home' );
      endif;
    ?>
	</div><!-- .entry-content -->
	</div>
</div>

</article><!-- #post-## -->

<?php include('contact.php'); ?>

</div>
</div>