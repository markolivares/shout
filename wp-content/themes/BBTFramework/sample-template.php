<?php
/**
 * Template Name: Sample
 *
 * This is the template for sample
 *
 * @package sparkling
 */

get_header(); ?>

<?php //$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 'post_type' => 'campaigns', 'posts_per_page' => 3, 'paged' => $paged );
$wp_query = new WP_Query($args);
while ( have_posts() ) : the_post(); ?>
    <h2><?php the_title() ?></h2>
<?php endwhile; ?>

<!-- then the pagination links -->

<?php next_posts_link( '&larr; Older posts', $wp_query ->max_num_pages); ?>
<?php previous_posts_link( 'Newer posts &rarr;' ); ?>



<?php get_footer(); ?>