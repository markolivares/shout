<?php

add_action( 'init', 'create_post_type_campaigns' );

function create_post_type_campaigns() {
  register_post_type( 'campaigns',
    array(
      'labels' => array(
        'name' => __( 'Campaigns' ),
        'singular_name' => __( 'Campaigns' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'thumbnail','page-attributes'  ),
      'taxonomies' => array('category')
    )
  );
}

add_action( 'init', 'create_post_type_squad' );

function create_post_type_squad() {
  register_post_type( 'squad',
    array(
      'labels' => array(
        'name' => __( 'Squad' ),
        'singular_name' => __( 'Squad' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'thumbnail','page-attributes'  ),
      'taxonomies' => array('category')
    )
  );
}


function register_my_menu() {
  register_nav_menu('secondary-menu',__( 'Secondary Menu' ));
}
add_action( 'init', 'register_my_menu' );

add_filter( 'gform_field_validation_1_5', 'custom_spam_validation', 10, 4 );

function custom_spam_validation( $result, $value, $form, $field  ){
  if( $result['is_valid'] && !empty($value) ){
    $result['is_valid'] = false;
    $result['message'] = 'Anti spam field was filled in';
  }
  return $result;
}

?>