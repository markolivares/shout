<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package sparkling
 */
?><!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="eMzMDm9cKmKKIJrQNiYNWj19vvDmVSEhbqlB8w6JQSk" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<script src="javascript.js"></script>
<script src="/wp-content/themes/BBTFramework/js/street-poster-hover.js"/></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="/wp-content/themes/BBTFramework/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="/wp-content/themes/BBTFramework/slick/slick-theme.css"/>

	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
	<script type="text/javascript" src="/wp-content/themes/BBTFramework/slick/slick.min.js"></script>
	<script type="text/javascript" src="/wp-content/themes/BBTFramework/slick/slick.js"></script>

	<link rel="icon" href="/wp-content/uploads/2015/04/favicon-32x32.png" type="image/x-icon" />
<link rel="shortcut icon" href="/wp-content/uploads/2015/04/favicon-32x32.png" type="image/x-icon" />

<?php wp_head(); ?>


<!-- Home Page -->
<?php
if (is_front_page()){
?>
<style>
@media (max-width: 992px){
	main#main{
		height: 1160px;
	}
}
</style>
<?php
}
?>


<?php

if(is_singular( 'campaigns' ) ){
	?>
	<style>
			li#menu-item-15 a{
				  color: #ffffff;
  background: url('/wp-content/uploads/2015/04/arrow_up.png');
  background-repeat: no-repeat;
  background-size: 50px 25px;
    background-position: 50% 65%;
			}
	li#menu-item-53 a {
	  color: #ffffff;
	  background: url('/wp-content/uploads/2015/04/arrow_up.png');
	  background-repeat: no-repeat;
	  background-size: 50px 25px;
	    background-position: 50% 65%;
	}

	</style>
	<?php
}?>

<?php
if(is_front_page()){?>
	<style type="text/css">
		li#menu-item-302 a {
			  color: #ffffff;
			  background: url('/wp-content/uploads/2015/04/arrow_up.png');
			  background-repeat: no-repeat;
			  background-size: 50px 25px;
			    background-position: 50% 65%;
			}
	</style>
<?php
}

?>


<?php
//if ( $post->post_parent == '9' ) {
if(in_category( 'media' ) || is_page('shout-live') || is_page('shout-connect') || is_page('motion') || is_page('convenience') || is_page('street') || is_page('cafe-media') || is_page('air-new-zealand')){
  ?>
  <style>
  li#menu-item-14 a{
  		  color: #ffffff;
	  background: url('/wp-content/uploads/2015/04/arrow_up.png');
	  background-repeat: no-repeat;
	  background-size: 50px 25px;
	    background-position: 50% 65%;
  }
  </style>
  <?php
}
?>

<!-- If it is shout squad -->
<?php
if (is_singular( 'squad' )) {
?>
<style>
@media (max-width: 480px){
  #media-featured-image img.single-featured.wp-post-image{
    width: 100%;
  }
  #media-featured-image{
    padding: 0;
    padding-bottom: 5%;
  }
}
</style>
<?php
}
?>


<?php
if (is_page( 'contact' )) {
?>
<style type="text/css">
#comments {
    display: none;
}

#main_media {
    width: 100%;
}

.main_media_text {
    width: 100%;
    float: none;
}

h1.entry-title {
    display: none;
}
.main_media_text {
    padding: 0;
}

.entry-content {
    overflow: hidden;
    max-height: none;
}

.vc_row.wpb_row.vc_row-fluid.contact-page {
    width: 60%;
    margin: auto;
    border: 10px solid white;
    border-radius: 35px;
    padding: 20px;
    text-align: center;
}

.vc_row.wpb_row.vc_row-fluid:nth-child(1) {
    display: none;
}

.vc_col-sm-6.contact-page-right.wpb_column.vc_column_container #contact-right{
	width:100%;
}

.vc_col-sm-6.contact-page-left.wpb_column.vc_column_container #contact-left{
	width: 100%;
}

#inner-layer-black, #inner-layer{
	top:0;
}

@media (max-width: 1200px){
	.vc_row.wpb_row.vc_row-fluid.contact-page {
    	padding: 8px;
	}

	.wpb_text_column.wpb_content_element, .wpb_raw_code.wpb_content_element.wpb_raw_html {
    	margin-bottom: 5px;
	}
}

@media (max-width: 992px){
	.vc_row.wpb_row.vc_row-fluid.contact-page {
    	padding: 20px;
		width: 90%;
	}
}

@media (max-width: 480px){
	.vc_row.wpb_row.vc_row-fluid.contact-page {
    	padding: 5px;
    	width: 90%;
	}
}
</style>
<?php
}
?>

<style type="text/css">
.navbar-nav > li > a{
	padding: 25px;
}
</style>


</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KPXLRK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KPXLRK');</script>
<!-- End Google Tag Manager -->
	
<div id="page" class="hfeed site">

	<header id="masthead" class="site-header" role="banner">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="row">
					<div class="site-navigation-inner col-sm-12">



		        <div class="navbar-header">
		            <button type="button" class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>

				<?php if( get_header_image() != '' ) : ?>

					<div id="logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php header_image(); ?>"  height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo( 'name' ); ?>"/></a>
					</div><!-- end of #logo -->

				<?php endif; // header image was removed ?>

				<?php if( !get_header_image() ) : ?>

					<div id="logo">
						
						<div id="shout_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img id="shout_logo_01" src="/wp-content/uploads/2015/11/shout_logo.png" alt="ShoutLogo"></a></div>
						<!--<span class="site-name"><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>-->
					</div><!-- end of #logo -->

				<?php endif; // header image was removed (again) ?>

		        </div>
					<?php sparkling_header_menu(); ?>
					</div>

					<div id="secondary-menu">
						<div id="arrow-down-menu"></div>
						<div id="arrow-down-menu-2"></div>
					</div>

						<div id="white">
						</div>

		    </div>
		  </div>


		</nav><!-- .site-navigation -->


	</header><!-- #masthead -->

	<div id="small-navigation">
							<!--<div id="inner-white">
								<?php
									if ( is_singular( 'campaigns' ) ) {
									    echo "CAMPAIGNS";
									}else{
										echo get_the_title();
									}
								?>
								<div id="inner-white-down">
								</div>
							</div>-->
							<div id='the-secondary-menu'>
									<div id="sec-menu">
										MENU
									</div>
									<div id="sec-title">
										<?php
											if (is_home()) {
												echo "CAMPAIGNS";
											}elseif ( is_singular( 'campaigns' ) ) {
											    echo "CAMPAIGNS";
											}elseif (is_singular( 'squad' )) {
												echo "SHOUT SQUAD";
											}elseif (in_category('media')) {
												echo "MEDIA";
											}elseif (is_page( 'Homepage' )) {
												echo "CAMPAIGNS";
											}
											else{
												echo get_the_title();
											}
										?>
									</div>
									<div id="inner-white-down-menu">
									</div>
									<div id="secondary-menu-li">
										<?php wp_nav_menu( array( 'theme_location' => 'secondary-menu' ) ); ?>
									</div>
							</div>
	</div>

	
	<div id="content" class="site-content">

		<div class="top-section">
			<?php sparkling_featured_slider(); ?>
			<?php sparkling_call_for_action(); ?>
		</div>

		<div class="container main-content-area">



			<div class="row">

				<!--<div class="main-content-inner <?php echo sparkling_main_content_bootstrap_classes(); ?> <?php echo of_get_option( 'site_layout' ); ?>">-->


 <div id="super-main">

					<div class="main-content-inner col-sm-12 col-md-12">


