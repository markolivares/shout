<?php
/**
 * Template Name: NewMedia Template
 *
 * This is the template for media
 *
 * @package sparkling
 */

get_header(); ?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$image = $image[0]; ?>
<?php endif; ?>

<style type="text/css">
#main_media_image{
    display: none;
}

.main_media_text {
    float: none;
    width: 100%;
    padding: 0%;
    padding-top: 0%;
    padding-right: 0%;
}

.entry-content {
    overflow: initial;
    margin: 0;
}

.vc_row.wpb_row.vc_inner.vc_row-fluid.media-title{
    background-image: url('<?php echo $image; ?>');
    background-size: 100%;
    padding: 5%;
    padding-left: 15%;
    padding-right: 15%;
}

.vc_row.wpb_row.vc_inner.vc_row-fluid.media-two-column {
    padding: 5%;
}

.vc_row.wpb_row.vc_inner.vc_row-fluid.media-two-column h2{
    font-family: 'Aleo-Regular';
    color: #ff0099;
    font-size: 20px;
    font-weight: 600;
    margin-top: -1.5%;
}

.media_page {
    /**height: 1850px;**/
    height:2000px;
}

.vc_row.wpb_row.vc_inner.vc_row-fluid.media-gallery {
    padding: 5%;
    background-color: #ffffff;
}

.wpb_content_element .wpb_gallery_slides ul li{
    position: static !important;
    padding: 1%;
}

.wpb_content_element .wpb_gallery_slides ul li:nth-child(1) img {
    width: 100% !important;
}

.wpb_content_element .wpb_gallery_slides ul li:nth-child(1) {
    width: 47.8%;
}

.wpb_content_element .wpb_gallery_slides ul li:nth-child(2) img {
    width: 100% !important;
}

.wpb_content_element .wpb_gallery_slides ul li:nth-child(2) {
    width: 48%;
}

.wpb_content_element .wpb_gallery_slides ul li:nth-child(3) img {
    width: 100% !important;
}

.wpb_content_element .wpb_gallery_slides ul li:nth-child(3) {
    width: 48%;
}

.wpb_content_element .wpb_gallery_slides ul li:nth-child(4) img,.wpb_content_element .wpb_gallery_slides ul li:nth-child(5) img, .wpb_content_element .wpb_gallery_slides ul li:nth-child(6) img{
    width: 100% !important;
}

.wpb_content_element .wpb_gallery_slides ul li:nth-child(4),.wpb_content_element .wpb_gallery_slides ul li:nth-child(5), .wpb_content_element .wpb_gallery_slides ul li:nth-child(6){
    width: 31.8%;
}

.wpb_image_grid .wpb_image_grid_ul a{
    position: relative;
    overflow: hidden;
    width: 100%;
}

.mediapic_hover{
    position: absolute;
    top: 0px;
    right: 0px;
    width: 100%;
    height: 100%;  
}

.mediapic_hover:hover {
    background-color: rgba(255,0,153,0.5);
}

.mediapic_text{
    position: relative;
    top: 50%;
    -webkit-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    transform: translateY(-50%);
    padding: 5%;
    text-align: center;
    font-family: 'Aleo-Regular';
    font-weight: 500;
    color: #ffffff;
    font-size: 22px;
    display: none;
}

/** pop up **/
div.pp_default .pp_top, div.pp_default .pp_top .pp_middle, div.pp_default .pp_top .pp_left, div.pp_default .pp_top .pp_right, div.pp_default .pp_bottom, div.pp_default .pp_bottom .pp_left, div.pp_default .pp_bottom .pp_middle, div.pp_default .pp_bottom .pp_right{
    display: none;
}

div.pp_default .pp_content_container .pp_right, div.pp_default .pp_content_container .pp_left{
    background: none !important;
    padding: 0 !important;
}

.ppt {
    background-color: #ff0099;
    padding: 2%;
    margin: 0 !important;
    width: 100% !important;
    position: inherit !important;
    bottom: 0 !important;
    color: #ff0099 !important;
}

div.pp_default .pp_content{
    width: 100% !important;
    height: inherit !important;
}

#pp_full_res img{
    width: 100% !important;
}

.pp_details {
    position: absolute !important;
    top: 0px !important;
    width: 100% !important;
}

.pp_nav {
    display: none !important;
}

.pp_gallery{
    margin-top: -120px !important;
}

.pp_hoverContainer{
    /**width: 100% !important;**/
}

a.pp_close{
    right: -15px !important;
    top: -20px !important;
}

div.pp_default .pp_close {
    width: 40px !important;
    height: 40px !important;
    background: url(/wp-content/uploads/2015/11/pinkx1.png) no-repeat !important;
    background-size: 100% 100% !important;
}

a.pp_previous, div.pp_default .pp_previous:hover, a.pp_next, div.pp_default .pp_next:hover{
    background: none !important;
}

/** for expand **/
a.pp_expand {
    display: none !important;
}

.entry-content{
    max-height: 100% !important;
}

@media(max-width: 1200px){
    .media_page {
        height: 1600px;
    }
}


@media(max-width: 992px){
    #media-featured-image, #media-featured-title{
        display: none;
    }

    #media-featured-content {
        padding-bottom: 20%;
        padding: 0%;
    }

    .vc_row.wpb_row.vc_inner.vc_row-fluid.media-title{
        background-size: cover;
    }

    .vc_row.wpb_row.vc_inner.vc_row-fluid.media-two-column .vc_col-sm-6.wpb_column.vc_column_container:nth-child(1){
        padding-bottom: 10%;
    }

    #inner-layer-black, #inner-layer{
        top:0px;
    }

    /**.media_page {
        height: 1480px;
    }**/

    article {
        display: table;
    }

    #main_media_small{
        display: table-row;
        width: auto;
    }

    .media_page {
        display: table;
        height: auto;
    }


}

@media (max-width: 480px){
    .wpb_content_element .wpb_gallery_slides ul li{
        width:100% !important;
    }

    .vc_row.wpb_row.vc_inner.vc_row-fluid.media-title{
        padding-left: 5%;
        padding-right: 5%;
    }

    .mediapic_hover{
        display: none;
    }

    .wpb_image_grid .wpb_image_grid_ul a{
        pointer-events: none;
        cursor: default;
    }
}


</style>




<script type="text/javascript">
    $( document ).ready(function() {
        $("a.prettyphoto").append( "<div class='mediapic_hover'><div class='mediapic_text'>Click to Expand</div> </div>" );
        $("a.prettyphoto").mouseover(function(){
            $(this).find(".mediapic_text").css("display","block");
        })
        $("a.prettyphoto").mouseout(function(){
            $(this).find(".mediapic_text").css("display","none");
        })
    });
</script>

    <div id="primary" class="content-area">

        <main id="main" class="site-main" role="main">

            <?php while ( have_posts() ) : the_post(); ?>

                <?php get_template_part( 'newmedia', 'page' ); ?>

            <?php endwhile; // end of the loop. ?>

        </main><!-- #main -->




        <div id="media-next-main">
                <div id="media-next-inner-main">
                MORE
                </div>
            </div>

        <div id="media-menu">
            
            <div id="media-next">
                <div id="media-next-inner">
                <img id="blue-arrow" src="/wp-content/uploads/2015/04/arrow_blue.png">
                <img id="blue-arrow" src="/wp-content/uploads/2015/04/arrow_blue.png">
                </div>
            </div>
                
            <div id="media-boxes">
                    
                    <a href='/street-posters/'>
                    <div id="media-box">
                        <div class="media-text">STREET POSTERS</div>
                    </div>
                    </a>


                    <a href='/digital-fuel-network/'>
                    <div id="media-box">
                        <div class="media-text">DIGITAL FUEL NETWORK</div>
                    </div>
                    </a>


                    <a href='/convenience/'>
                    <div id="media-box">
                        <div class="media-text">PATH TO PURCHASE</div>
                    </div>
                    </a>

                    <a href='/air-new-zealand/'>
                    <div id="media-box">
                        <div class="media-text">AIR NEW ZEALAND</div>
                    </div>
                    </a>

                    <a href='/street/'>
                    <div id="media-box">
                        <div class="media-text">STREET</div>
                    </div>
                    </a>

                    <a href='/cafe-media/'>
                    <div id="media-box">
                        <div class="media-text">CAFE MEDIA</div>
                    </div>
                    </a>

                    <a href='/motion/'>
                    <div id="media-box">
                        <div class="media-text">MOTION</div>
                    </div>
                    </a>

                    <a href='/shout-connect/'>
                    <div id="media-box">
                        <div class="media-text">SHOUT ACTIVATE</div>
                    </div>
                    </a>


                    <!--<a href='/shout-live/'>
                    <div id="media-box">
                        <div class="media-text">SHOUT LIVE</div>
                    </div>
                    </a>-->
            </div>

        </div>

    </div><!-- #primary -->





<?php get_footer(); ?>
